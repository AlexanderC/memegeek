/**
 * @author AlexanderC
 */

$().ready(function(){
   $(".spoiler-gutter").click(function(){
       $(".add-meme").slideToggle("fast");
   });

    $('#meme-file').bind("change", function(e){
        $("#meme-edit").html("");

        if($(this).val() == "") {
            $("#meme-submit").hide();
            return;
        }

        window.loadImage(
            e.target.files[0],
            function (img) {
                $('#meme-edit').append(img);
                $(img).textOverCanvas();
                $("#meme-submit").fadeIn("fast");
            },
            {
                maxWidth: 600,
                maxHeight: 300,
                minWidth: 100,
                minHeight: 50,
                canvas: true,
                noRevoke: true
            }
        );
    });

    $("#meme-submit > input:submit").click(function(){
        var data = $("#meme-edit > canvas")[0].toDataURL("image/png");

        $.ajax({
            url: "/upload.php",
            processData: false,
            type: "POST",
            data: data,
            success: function(data){
                if(data == "OK") {
                    alert("Thanks for adding this awesome meme!");
                    $(".add-meme").slideUp("fast");
                } else {
                    alert("Sorry, we are experiencing some problems on the server now. Please try after few seconds.");
                    console.log(data);
                }
            }
        });
    });

    // first mansonry
    $(".meme-item > img").load(function(){
        $('#memes').masonry({
            itemSelector: '.meme-item',
            isAnimated: true,
            gutterWidth: 5,
            isResizable: true,
            isAnimatedFromBottom: true
        });
    });


    $('#memes').infinitescroll({
            loading: {
                finishedMsg: "That's all for now! Would be great if you'll add another memes ;)",
                img: 'http://i.imgur.com/6RMhx.gif',
                msgText: "<em>Loading the next set of memes...</em>",
            },
            navSelector  : "div.navigation",
            nextSelector : "div.navigation a:first",
            itemSelector : ".meme-item",
            debug: false
        },
        function(newElements) {
            var $newElems = $(newElements).css({ opacity: 0 });
            $newElems.imagesLoaded(function() {
                $newElems.animate({ opacity: 1 });
                $('#memes').masonry('appended', $newElems, true);
            });
        }
    );
});