
Templatic Ecommerce Glyphs
Free Icon Set of 50 Glyphs 

---

Website: http:/templatic.com

---

This Set was brought to you by  by Templatic.com and 
is licensed under a Creative Commons Attribution-By 3.0 Unported License 
(http://creativecommons.org/licenses/by/3.0/). 

Make sure you share these Icons using this link only: 

http://templatic.com/news/free-templatic-ecommerce-glyphs-icons

---

Templatic.com is a dynamic team of WordPress professionals helping you 
build professional websites & online portals quick, easy and affordably. 





