<?php

/**
 * @author AlexanderC
 */

class MemeManager
{
    const DEFAULT_FORMAT = 'png';
    const MAX_PER_SUBDIR = 20;

    /**
     * @var string
     */
    private $storageDir;

    /**
     * @param string $storageDir
     */
    public function __construct($storageDir)
    {
        $this->storageDir = rtrim($storageDir, '/') . '/';
    }

    /**
     * @param int $page
     * @return array
     */
    public function getMemes($page = 1) {
        $page = (int) $page <= 0 ? 1 : (int) $page;
        $dir =  $this->storageDir . ((int) end(scandir($this->storageDir, SCANDIR_SORT_ASCENDING)) - ($page - 1));

        if(!is_dir($dir)) {
            throw new \UnexpectedValueException("Directory {$dir} does not exists");
        }
        return new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS);
    }

    /**
     * @param string $img
     * @param string $format
     * @throws UnexpectedValueException
     * @return bool
     */
    public function save($img, $format = self::DEFAULT_FORMAT)
    {
        $tmp = tempnam(sys_get_temp_dir(), "meme_");
        rename($tmp, "{$tmp}.{$format}");
        $tmp = "{$tmp}.{$format}";

        if(!@file_put_contents($tmp, $img, LOCK_EX)) {
            throw new \RuntimeException("Unable to write into {$tmp} file");
        }

        if(!$this->isImage($tmp)) {
            throw new \UnexpectedValueException("Given string is not an image");
        }

        return rename($tmp, $this->getCurrentSubfolder() . '/' . basename($tmp));
    }

    /**
     * @return string
     */
    public function getCurrentSubfolder()
    {
        $dir = end(scandir($this->storageDir, SCANDIR_SORT_ASCENDING));

        if($dir == '.' || $dir == '..' || (empty($dir) && $dir != 0)) {
            $dir = $this->storageDir . 0;

            if(!mkdir($dir)) {
                throw new \RuntimeException("Unable to create {$newDir} directory");
            }

            return $dir;
        }

        if(count(scandir($this->storageDir . $dir)) - 2 /* . && .. */  >= self::MAX_PER_SUBDIR) {
            $newDir = $this->storageDir . ((int) $dir + 1);

            if(!mkdir($newDir)) {
                throw new \RuntimeException("Unable to create {$newDir} directory");
            }

            return $newDir;
        }

        return $this->storageDir . $dir;
    }


    /**
     * @param $img
     * @return bool
     */
    public function isImage($img)
    {
        return (bool) @getimagesize($img);
    }
}
