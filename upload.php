<?php

/**
 * @author AlexanderC
 */

$config = require __DIR__.'/config.php';
$manager = new MemeManager($config['storage']);

$data = file_get_contents('php://input');

if(!empty($data)) {
    try {
        $content = base64_decode(str_replace("data:image/png;base64,", "", $data));
        $manager->save($content);
    } catch(\Exception $e) {
        exit($e);
    }
}

@ob_end_clean();
exit('OK');