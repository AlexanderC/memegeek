<?php

/**
 * @author AlexanderC
 */

$config = require __DIR__.'/config.php';
$manager = new MemeManager($config['storage']);
$page = isset($_REQUEST['p']) ? (int) $_REQUEST['p'] : 1;

foreach($manager->getMemes($page) as $meme) {
    echo "<div class='meme-item' onclick='window.location=\"/meme.php?f=" . ($page - 1) . "/{$meme->getBasename()}\"'>";
        ob_start();
        passthru("cat " . escapeshellarg($meme->getPathname()));
        echo "<img src='data:image/png;base64," . base64_encode(ob_get_clean()) . "'/>";
    echo "</div>";
}