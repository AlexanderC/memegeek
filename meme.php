<?php

/**
 * @author AlexanderC
 */

if(!isset($_REQUEST['f'])) {
    @ob_end_clean();
    header($_SERVER['SERVER_PROTOCOL'] . " 404 Not Found");
    exit;
}

$config = require __DIR__.'/config.php';
$path = realpath(rtrim($config['storage'], '/') . "/" . trim($_REQUEST['f'], '/'));

if(strpos($path, rtrim(realpath($config['storage']), '/') . "/") !== 0) {
    @ob_end_clean();
    header($_SERVER['SERVER_PROTOCOL'] . " 404 Not Found");
    exit;
}

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>MemeGEEK | GEEKifying the world ;)</title>
	<meta name="description" content="MemeGEEK | GEEKifying the world ;)">
	<meta name="author" content="AlexanderC">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link rel="stylesheet" href="/stylesheets/base.css">
	<link rel="stylesheet" href="/stylesheets/skeleton.css">
	<link rel="stylesheet" href="/stylesheets/layout.css">
	<link rel="stylesheet" href="/stylesheets/main.css">

    <script src="/js/jq-1.7.1.js"></script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({publisher: "0871b9ee-6142-4932-a868-61115c283b9d", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

</head>
<body>
	<div class="container">
		<div class="sixteen columns">
            <?php include __DIR__.'/header.php'; ?>
			<hr />
            <div class="meme-item-wrapper">
                <div class='meme-item-big'>
<?php
            ob_start();
            passthru("cat " . escapeshellarg($path));
            echo "<img src='data:image/png;base64," . base64_encode(ob_get_clean()) . "'/>";
?>
                    <hr/>
                    <span class='st_facebook_hcount' displayText='Facebook'></span>
                    <span class='st_twitter_hcount' displayText='Tweet'></span>
                    <span class='st_googleplus_hcount' displayText='Google +'></span>
                    <span class='st_pinterest_hcount' displayText='Pinterest'></span>
                    <span class='st_odnoklassniki_hcount' displayText='Odnoklassniki'></span>
                    <span class='st_vkontakte_hcount' displayText='Vkontakte'></span>
                    <hr/>
                    <?php include __DIR__.'/footer.php' ?>
                </div>
            </div>
		</div>
	</div>
</body>
</html>


