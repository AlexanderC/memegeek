<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>MemeGEEK | GEEKifying the world ;)</title>
	<meta name="description" content="MemeGEEK | GEEKifying the world ;)">
	<meta name="author" content="AlexanderC">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link rel="stylesheet" href="/stylesheets/base.css">
	<link rel="stylesheet" href="/stylesheets/skeleton.css">
	<link rel="stylesheet" href="/stylesheets/layout.css">
    <link rel="stylesheet" href="/js/jquery-ui/jquery-ui-1.10.0.custom.min.css">
	<link rel="stylesheet" href="/stylesheets/main.css">

    <script src="/js/jq-1.7.1.js"></script>
    <script src="/js/masonry.js"></script>
    <script src="/js/li.js"></script>
    <script src="/js/jquery-ui/jquery-ui-1.10.0.custom.min.js"></script>
    <script src="/js/textOverCanvas.js"></script>
    <script src="/js/infinitescroll.js"></script>
    <script src="/js/main.js"></script>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

</head>
<body>
	<div class="container">
		<div class="sixteen columns">
            <?php include __DIR__.'/header.php'; ?>
			<hr />
            <div class="spoiler-gutter"><img src="/images/e_icons/Alert.png"/>Help us by adding some awesome memes...<img src="/images/e_icons/Alert.png"/></div>
            <div class="fifteen columns add-meme" style="display:none">
                <div class="one-third column">
                    <input type="file" id="meme-file"/>
                </div>
                <div class="two-thirds columns" id="meme-edit">

                </div>
                <div class="fourteen columns" id="meme-submit">
                    <input type="submit" value="Add This One!">
                </div>
            </div>
            <hr/>
            <div id="memes">
                <?php require __DIR__.'/memes.php'; ?>
                <div class="navigation"><a href="/memes.php?p=2"></a></div>
            </div>
            <hr/>
            <?php include __DIR__.'/footer.php' ?>
		</div>
	</div>
</body>
</html>
