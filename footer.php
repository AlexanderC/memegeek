<?php

/**
 * @author AlexanderC
 */ 

?>

<div class='footer'>
	Developed by <a href="https://www.alexanderc.me/">AlexanderC</a>. All Right Reserved <?= date("Y") ?>.
</div>	
