/**
 * @author AlexanderC
 */

(function ($) {
    $.fn.extend({
        textOverCanvas:function (options) {
            var defaults = {
                textWrapperCss:{
                    background:"#fff",
                    border:"5px dotted #fff",
                    width:"60%",
                    background:"none"
                },
                textSaveCss:{
                    width:"100%",
                    height:"30px",
                    margin:0,
                    padding:0
                },
                textareaCss:{
                    width:"95%",
                    height:"40px",
                    background:"transparent",
                    border:"none",
                    margin:"2px",
                    "line-height": "1em"
                },
                colorInputCss:{
                    float: "left",
                    height:"15px",
                    width:"80%",
                    top:0,
                    left:0,
                    padding:0,
                    border:"none"
                },
                sizeInputCss:{
                    float: "right",
                    height:"15px",
                    width:"20%",
                    top:0,
                    left:0,
                    padding:0,
                    border:"none",
                    "text-align": "center"
                },
                saveTextLabel: "Save Text",
                defaultTextSize: 25,
                defaultTextColor: "#FFFFFF",
                textLineHeight: 1
            }

            var options = $.extend(defaults, options);

            var self = this;

            this.stripos = function(f_haystack, f_needle, f_offset) {
                // http://kevin.vanzonneveld.net
                // +     original by: Martijn Wieringa
                // +      revised by: Onno Marsman
                // *         example 1: stripos('ABC', 'a');
                // *         returns 1: 0
                var haystack = (f_haystack + '').toLowerCase();
                var needle = (f_needle + '').toLowerCase();
                var index = 0;

                if ((index = haystack.indexOf(needle, f_offset)) !== -1) {
                    return index;
                }
                return false;
            };

            this.getDefaultLineHeight = function(fontSize) {
                var h = fontSize * (fontSize / 30);
                console.log(h);
                return h;
            };

            return this.each(function () {
                var el = $(this);
                var canvas = $(this)[0];
                var pos = $(this).position();

                var cont = $("<div></div>");
                cont.addClass("text-over-canvas");

                $(this).after(cont);

                cont.css({
                    position:"absolute",
                    left:pos.left,
                    top:pos.top,
                    background:"transparent",
                    padding:0,
                    margin:0,
                    height:$(this).innerHeight(),
                    width:$(this).innerWidth(),
                    border:"none"
                });

                textWrapper = $("<div></div>");
                textWrapper.css(options.textWrapperCss);

                textSave = $("<button class='toc-save'>" + options.saveTextLabel + "</button>");
                textSave.css(options.textSaveCss);

                textColor = $("<input type='color' value='" + options.defaultTextColor +"'/>");
                textColor.css(options.colorInputCss);

                textSize = $("<input type='number' value='" + options.defaultTextSize +"'/>");
                textSize.css(options.sizeInputCss);

                text = $("<textarea></textarea>");
                text.css(options.textareaCss);
                text.css({
                    "font-size": options.defaultTextSize,
                    color: options.defaultTextColor
                });

                textWrapper.append(textColor);
                textWrapper.append(textSize);
                textWrapper.append(text);
                textWrapper.append(textSave);
                cont.append(textWrapper);
                textWrapper.draggable({containment:cont, scroll:false});

                textColor.change(function () {
                    text.css({
                        color:$(this).val()
                    });
                });

                textSize.change(function () {
                    text.css({
                        "font-size": $(this).val() + "px"
                    });
                });

                textSave.click(function () {
                    var content = text.val();
                    var font = text.css("font");
                    var Toff = textWrapper.offset();
                    var EOff = el.offset();
                    var color = text.css("color");
                    var fontSize = parseInt(text.css("font-size"));

                    var context = canvas.getContext('2d');
                    context.font = font;
                    context.strokeStyle = color;
                    context.fillStyle = color;

                    var left = Math.abs(EOff.left - Toff.left) +
                                parseInt(textWrapper.css("padding-left")) +
                                parseInt(textWrapper.css("border-left-width")) +
                                parseInt(text.css("padding-left")) +
                                parseInt(text.css("margin-left"))
                        ;
                    var top = Math.abs(EOff.top - Toff.top) +
                                (Math.ceil(fontSize / 10) * parseInt(text.css("padding-top"))) +
                                parseInt(textWrapper.css("padding-top")) +
                                parseInt(textWrapper.css("border-top-width")) +
                                parseInt(textColor.outerHeight()) +
                                parseInt(textSize.outerHeight()) +
                                parseInt(text.css("margin-top"))
                        ;

                    var contArr = content.split("\n");
                    var lineHeight = ((fontSize * options.textLineHeight) / 11);

                    text.val("");

                    for(line in contArr) {
                        // start very crazy thing
                        var topCoef = (line * (fontSize + lineHeight)) - ((fontSize / 8) * line + (fontSize / (3.8 + line)));
                        // end very crazy thing
                        context.fillText(contArr[line], left, top + topCoef);
                    }

                    context.save();
                });
            });
        }
    });
})(jQuery);
